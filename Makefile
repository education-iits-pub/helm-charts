include Makefile.variables

#update chart version based on git version and tag and update dependencyies
update-charts:
	@for HELM_CHART in $(HELM_CHARTS) ; do \
	    make --no-print-directory package-and-update-chart SERVICE_NAME=$${HELM_CHART}; \
	done

package-and-update-chart:
	@helm package $(SERVICE_NAME) --version $(RELEASE_VERSION) -u
	@helm chart save $(SERVICE_NAME)-$(RELEASE_VERSION).tgz registry.gitlab.com/education-group-iits/helm-charts/$(SERVICE_NAME) >/dev/null
	@helm chart export registry.gitlab.com/education-group-iits/helm-charts/$(SERVICE_NAME):$(RELEASE_VERSION) >/dev/null
	@rm $(SERVICE_NAME)-$(RELEASE_VERSION).tgz